using System;

public interface IReactiveProperty<TValue>
{
    TValue Value {get;}
    event Action<TValue> OnValueChanged;
}
