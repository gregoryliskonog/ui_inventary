using System;

public class ReactiveProperty<TValue> : IReactiveProperty<TValue>
{
    private TValue value;

    public TValue Value
    {
        get => value;
        private set
        {
            var previousValue = this.value;
            this.value = value;

            if (!previousValue.Equals(value))
                onValueChanged?.Invoke(value);
        }
    }
    
    private event Action<TValue> onValueChanged;
    public event Action<TValue> OnValueChanged
    {
        add
        {
            onValueChanged += value;
            onValueChanged?.Invoke(this.value);
        }
        remove => onValueChanged -= value;
    }

    public ReactiveProperty(TValue initialValue)
    {
        this.value = initialValue;
    }

    public override string ToString()
    {
        return value.ToString();
    }

    public void SetValue(TValue value)
    {
        Value = value;
    }

}
