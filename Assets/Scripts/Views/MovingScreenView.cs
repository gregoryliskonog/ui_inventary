using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class MovingScreenView : MonoBehaviour, IMovingScreenView
{
    [SerializeField]
    private Button moveButton;
    public UnityEvent OnMoveButtonClick => moveButton.onClick;


    public void SetMoveButtonActiveState(bool isActive)
    {
        moveButton.interactable = isActive;
    }
}
