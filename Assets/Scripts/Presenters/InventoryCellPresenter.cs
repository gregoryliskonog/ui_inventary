using System;

public class InventoryCellPresenter : IDisposable, IInitializable
{
    private readonly InventoryCell inventoryCell;
    private readonly MovingScreen movingItemHolder;
    private readonly IInventoryCellView view;

    public InventoryCellPresenter(InventoryCell inventoryCell, MovingScreen movingItemHolder, IInventoryCellView view)
    {
        this.inventoryCell = inventoryCell;
        this.movingItemHolder = movingItemHolder;
        this.view = view;
    }

    public void Init()
    {
        inventoryCell.CellItem.OnValueChanged += InventoryCell_CellItemValueChangedListener;
        movingItemHolder.SelectedCell.OnValueChanged += MovingItemHolder_SelectedCellValueChangedListener;
        view.OnClickCell.AddListener(InventoryCellView_OnClickCellListener);
        view.SetActivityStatus(true);
    }

    public void Dispose()
    {
        inventoryCell.CellItem.OnValueChanged -= InventoryCell_CellItemValueChangedListener;
        movingItemHolder.SelectedCell.OnValueChanged -= MovingItemHolder_SelectedCellValueChangedListener;
        view.OnClickCell.RemoveListener(InventoryCellView_OnClickCellListener);
    }

    private void SetSelectedState(bool isSelected)
    {
        view.SetSelectedState(isSelected);
    }

    private void SetItem(Option<InventoryItem> inventoryItem)
    {
        view.SetSprite(inventoryItem.Map(item => item.ItemSprite));
    }

    private void InventoryCellView_OnClickCellListener()
    {
        movingItemHolder.SelectCell(inventoryCell);
    }

    private void MovingItemHolder_SelectedCellValueChangedListener(Option<InventoryCell> selectedCell)
    {
        var isCellSelected = selectedCell
            .When(
                onSome: cell => cell == inventoryCell,
                onNone: () => false
            );

        SetSelectedState(isCellSelected);
    }

    private void InventoryCell_CellItemValueChangedListener(Option<InventoryItem> cellItem)
    {
        SetItem(cellItem);
    }


}
