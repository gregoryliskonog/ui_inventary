using System;

public static class OptionExtensions
{
    public static void Fold<T>(this Option<T> option, Action<T> onSome = null, Action onNone = null)
    {
        if (option.TryGetValue(out var value))
            onSome?.Invoke(value);
        else
            onNone?.Invoke();
    }

    public static TResult When<TResult, TValue>(this Option<TValue> option, Func<TValue, TResult> onSome, Func<TResult> onNone)
    {
        if (option.TryGetValue(out var value))
            return onSome(value);
        else
            return onNone();

    }

    public static Option<TResult> Map<TValue, TResult>(this Option<TValue> option, Func<TValue, TResult> mapper)
    {
        if (option.TryGetValue(out var value))
            return Option.Some(mapper(value));
        else
            return Option<TResult>.None;
    }

    public static Option<TValue> Combine<TValue>(this Option<TValue> option, Option<TValue> anotherOption, Func<TValue, TValue, TValue> onBoth)
    {
        if (option.TryGetValue(out var value))
        {
            if (anotherOption.TryGetValue(out var anotherValue))
                return Option.Some(onBoth(value, anotherValue));


            return option;
        }

        return anotherOption;
    }
}
