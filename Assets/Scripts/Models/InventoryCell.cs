using UnityEngine;

public class InventoryCell
{
    public readonly string Name;
    private readonly ReactiveProperty<Option<InventoryItem>> cellItem;

    public bool Empty => !CellItem.Value.HasValue;
    public IReactiveProperty<Option<InventoryItem>> CellItem => cellItem;

    public InventoryCell(Option<InventoryItem> item, string name)
    {
        Name = name;
        cellItem = new ReactiveProperty<Option<InventoryItem>>(item);
    }

    public bool HasItem(InventoryItem item)
    {
        if (cellItem.Value.TryGetValue(out var currentItem))
            return currentItem == item;

        return false;
    }

    public override string ToString()
    {
        return $"{Name}_with_item_{cellItem}";
    }

    public void PlaceItem(InventoryItem item)
    {
        if (!cellItem.Value.HasValue)
            cellItem.SetValue(Option.Some(item));
        else
            Debug.LogError("Cannot put item to full cell");
    }

    public bool TryRemoveItem(out InventoryItem item)
    {
        if (cellItem.Value.TryGetValue(out item))
        {
            cellItem.SetValue(Option<InventoryItem>.None);
            return true;
        }

        Debug.LogWarning("Cannot get item from empty cell!");
        return false;
    }

    public bool TryRemoveItem()
    {
        if (cellItem.Value.HasValue)
        {
            cellItem.SetValue(Option<InventoryItem>.None);
            return true;
        }

        return false;
    }


}
