using UnityEngine;
using UnityEngine.Events;

public interface IInventoryCellView
{
    UnityEvent OnClickCell { get; }
    void SetActivityStatus(bool isActive);
    void SetSelectedState(bool isSelected);
    void SetSprite(Option<Sprite> sprite);
}
