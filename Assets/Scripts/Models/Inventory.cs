using System.Collections.Generic;
using UnityEngine;

public class Inventory
{
    public readonly string Name;

    private readonly List<InventoryCell> cells;

    public IReadOnlyList<InventoryCell> Cells => cells;

    public Inventory(int avaliableCells, List<InventoryItem> items, string name)
    {
        Name = name;

        cells = new List<InventoryCell>(avaliableCells);

        for (int i = 0; i < items.Count; i++)
        {
            var item = items[i];

            cells.Add(new InventoryCell(
                item: Option.Some(item), 
                name: $"{name}_inventory_cell_{i}"
            ));            
        }

        for (int i = items.Count; i < avaliableCells; i++)
        {
            cells.Add(new InventoryCell(
                item: Option<InventoryItem>.None, 
                name: $"{name}_inventory_cell_{i}"
            ));
        }
    }

    public bool HasCell(InventoryCell cell)
    {
        return cells.Contains(cell);
    }

    public override string ToString()
    {
        return $"inventory_{Name}";
    }

    public Option<InventoryCell> GetItemCell(InventoryItem item)
    {
        foreach (var cell in cells)
        {
            if (cell.HasItem(item))
                return Option.Some(cell);
        }

        return Option<InventoryCell>.None;
    }

    public bool TryAddItem(InventoryItem item)
    {
        foreach (var cell in cells)
        {
            if (cell.Empty)
            {
                cell.PlaceItem(item);
                return true;
            }
        }

        Debug.LogWarning("Cannot add item to inventory - inventory is full");
        return false;
    }

    public bool TryRemoveItem(InventoryItem item)
    {
        foreach (var cell in cells)
        {
            if (cell.HasItem(item))
                return cell.TryRemoveItem();
        }

        return false;
    }
}
