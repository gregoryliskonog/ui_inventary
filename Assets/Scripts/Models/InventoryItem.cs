using UnityEngine;

public class InventoryItem
{
    public readonly Sprite ItemSprite;
    public readonly string Name;


    public InventoryItem(Sprite itemSprite)
    {
        ItemSprite = itemSprite;
        Name = itemSprite.name;
    }

    public override string ToString()
    {
        return $"inventory_item_{Name}";
    }
}
