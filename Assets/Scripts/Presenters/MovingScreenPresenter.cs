using System;

public class MovingScreenPresenter : IDisposable, IInitializable
{
    private readonly MovingScreen movingItemHolder;
    private readonly IMovingScreenView view;

    public MovingScreenPresenter(MovingScreen movingItemHolder, IMovingScreenView view)
    {
        this.movingItemHolder = movingItemHolder;
        this.view = view;
    }

    public void Init()
    {
        this.movingItemHolder.SelectedCell.OnValueChanged += MovingItemHolder_SelectCellValueChangedListener;
        view.OnMoveButtonClick.AddListener(MovingScreenView_MoveButtonClickListener);
    }

    public void Dispose()
    {
        this.movingItemHolder.SelectedCell.OnValueChanged -= MovingItemHolder_SelectCellValueChangedListener;
        view.OnMoveButtonClick.RemoveListener(MovingScreenView_MoveButtonClickListener);
    }

    private void MovingScreenView_MoveButtonClickListener()
    {
        movingItemHolder.MoveItem();
    }

    private void MovingItemHolder_SelectCellValueChangedListener(Option<InventoryCell> selectedCell)
    {
        OnSelectedCellChange(selectedCell);
    }
    
    private void OnSelectedCellChange(Option<InventoryCell> cell)
    {
        var moveButtonActive = cell.When(
            onSome: _ => true,
            onNone: () => false
        );

        view.SetMoveButtonActiveState(moveButtonActive);
    }
}