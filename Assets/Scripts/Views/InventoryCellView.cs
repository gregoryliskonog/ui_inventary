﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class InventoryCellView : MonoBehaviour, IInventoryCellView
{
    [SerializeField]
    private Image backgroundImage;
    [SerializeField]
    private Image itemImage;
    [SerializeField]
    private GameObject selectedGameObject;
    [SerializeField]
    private Color inactiveColor = new Color(r: 1, g: 1, b: 1, a: 0.5f);
    [SerializeField]
    private Button cellButton;

    public UnityEvent OnClickCell => cellButton.onClick;

    private void Awake()
    {
        SetActivityStatus(false);
        SetSelectedState(false);
        SetSprite(Option<Sprite>.None);
    }

    public void SetActivityStatus(bool isActive)
    {
        backgroundImage.color = isActive ? Color.white : inactiveColor;
    }

    public void SetSelectedState(bool isSelected)
    {
        if (selectedGameObject.activeSelf != isSelected)
            selectedGameObject.SetActive(isSelected);
        
    }

    public void SetSprite(Option<Sprite> itemSprite)
    {
        itemSprite.Fold(
            onSome: sprite =>
            {
                itemImage.gameObject.SetActive(true);
                itemImage.sprite = sprite;
            },
            onNone: () =>
            {
                itemImage.sprite = null;
                itemImage.gameObject.SetActive(false);
            }
        );
    }


}
