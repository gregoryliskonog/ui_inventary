using UnityEngine;

public class MovingScreen
{
    private readonly Inventory playerInventory;
    private readonly Inventory otherInventory;
    private readonly ReactiveProperty<Option<InventoryCell>> selectedCell;

    public IReactiveProperty<Option<InventoryCell>> SelectedCell => selectedCell;

    public MovingScreen(Inventory playerInventory, Inventory otherInventory)
    {
        this.playerInventory = playerInventory;
        this.otherInventory = otherInventory;
        selectedCell = new ReactiveProperty<Option<InventoryCell>>(Option<InventoryCell>.None);
    }

    public void SelectCell(InventoryCell cell)
    {
        if (cell.Empty)
        {
            selectedCell.SetValue(Option<InventoryCell>.None);
            return;
        }

        if (selectedCell.Value.TryGetValue(out var oldCell))
        {
            if (oldCell == cell)
            {
                selectedCell.SetValue(Option<InventoryCell>.None);
                return;
            }
        }

        selectedCell.SetValue(Option.Some(cell));
    }

    public void MoveItem()
    {
        if (!selectedCell.Value.TryGetValue(out var cell))
        {
            Debug.LogWarning("Cannot move item - move item holder is empty");
            return;
        }

        if (!cell.TryRemoveItem(out var item))
        {
            Debug.LogError("Cannot move item - Selected cell is empty");
            return;
        }

        Inventory anotherInventory;

        if (playerInventory.HasCell(cell))
            anotherInventory = otherInventory;
        else
            anotherInventory = playerInventory;

        if (anotherInventory.TryAddItem(item))
            selectedCell.SetValue(Option<InventoryCell>.None);
        else
            Debug.LogWarning($"Cannot move item - inventory {anotherInventory.Name} is full");

    }

}
