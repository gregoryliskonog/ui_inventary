using System;


public struct Option<T> : IEquatable<Option<T>>
{
    public static Option<T> Some(T value) => new Option<T>(hasValue: true, value);
    public static Option<T> None = new Option<T>(hasValue: false, value: default(T));

    private readonly T value;
    private readonly bool hasValue;
    
    public bool HasValue => hasValue;
    
    private Option(bool hasValue, T value)
    {
        this.hasValue = hasValue;
        this.value = value;
    }
    
    public bool TryGetValue(out T value)
    {
        value = this.value;
        return hasValue;
    }

    public override string ToString()
    {
        if (hasValue)
        {
            return $"Option({value})";
        }
        else
        {
            return "None";
        }
    }

    public bool Equals(Option<T> other)
    {
        if (this.hasValue && other.hasValue)
        {
            return this.value.Equals(other.value);
        }
        else if (!this.hasValue && !other.hasValue)
        {
            return true;
        }

        return false;
    }
}

public static class Option
{
    public static Option<T> Some<T>(T value) => Option<T>.Some(value);
}