using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class EntryPoint : MonoBehaviour
{
    [SerializeField]
    private List<Sprite> playerInitialItems;
    [SerializeField]
    private List<Sprite> otherInitialItems;
    [SerializeField]
    private InventoryView playerInventoryView;
    [SerializeField]
    private InventoryView otherInventoryView;
    [SerializeField]
    private MovingScreenView movingScreenView;
    [SerializeField]
    private int avaliableCells = 10;

    private List<IDisposable> disposables = new List<IDisposable>();

    private void Start()
    {
        var playerItems = playerInitialItems.Select(sprite => new InventoryItem(sprite)).ToList();
        var otherItems = otherInitialItems.Select(sprite => new InventoryItem(sprite)).ToList();

        var playerInventory = new Inventory(
            avaliableCells,
            playerItems,
            name: "player"
        );

        var otherInventory = new Inventory(
            avaliableCells,
            otherItems,
            name: "other"
        );

        var movingItemHolder = new MovingScreen(playerInventory, otherInventory);

        SetInventoryCells(playerInventory, playerInventoryView, movingItemHolder);
        SetInventoryCells(otherInventory, otherInventoryView, movingItemHolder);

        var movingItemHolderPresenter = new MovingScreenPresenter(movingItemHolder, movingScreenView);
        movingItemHolderPresenter.Init();
        disposables.Add(movingItemHolderPresenter);

    }

    private void SetInventoryCells(Inventory inventory, InventoryView inventoryView, MovingScreen movingItemHolder)
    {
        for (int i = 0; i < inventory.Cells.Count; i++)
        {
            var cellView = inventoryView.InventoryCellViews[i];
            var cell = inventory.Cells[i];
            var presenter = new InventoryCellPresenter(cell, movingItemHolder, cellView);
            presenter.Init();
            disposables.Add(presenter);
        }
    }

    private void OnDestroy()
    {
        foreach (var disposable in disposables)
        {
            disposable.Dispose();
        }
    }
}
