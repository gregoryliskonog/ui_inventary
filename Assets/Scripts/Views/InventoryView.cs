using System.Collections.Generic;
using UnityEngine;

public class InventoryView : MonoBehaviour 
{
    [SerializeField]
    private List<InventoryCellView> inventoryCellViews;

    public IReadOnlyList<InventoryCellView> InventoryCellViews => inventoryCellViews;
}
