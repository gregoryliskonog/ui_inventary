using UnityEngine.Events;

public interface IMovingScreenView
{
    UnityEvent OnMoveButtonClick { get; }
    void SetMoveButtonActiveState(bool isActive);
}
